-- Answers to Exercise 8 here
ALTER TABLE dbtest_exercise05 DROP COLUMN email;

DELETE FROM dbtest_exercise05 WHERE username = 'btaylor45';

DROP TABLE dbtest_exercise05;

UPDATE dbtest_video_store
SET title = 'Jumanji'
WHERE barcode = 345964112;

UPDATE dbtest_video_store
SET rate = 6
WHERE barcode = 245789766;

UPDATE dbtest_video_store
SET  barcode = 191985325
WHERE title = 'Catfish';