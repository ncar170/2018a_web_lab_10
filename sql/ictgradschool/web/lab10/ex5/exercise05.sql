-- Answers to Exercise 5 here

-- CREATE TABLE IF NOT EXISTS dbtest_exercise05 (
 -- username VARCHAR(12) NOT NULL,
 -- fname VARCHAR(15),
 -- lname VARCHAR(20),
 -- email VARCHAR(50),
 -- PRIMARY KEY (username)
-- );

-- INSERT INTO dbtest_tablethree VALUES
 -- ('jsmith25', 'John', 'Smith', 'john@live.com'),
 -- ('btaylor45', 'Bill', 'Taylor', 'bt23@gmail.com'),
--  ('hrc15', 'Harry', 'Clinton', 'hrc23@hotmail.com'),
 -- ('pp25', 'Peter', 'Parker', 'peterp@hello.com'),
 -- ('pete15', 'Pete', 'Smith', 'psmith@live.com'),
 -- ('p3p', 'Paul', 'Peterson', 'ppp@gmail.com');


-- SELECT * FROM dbtest_tablethree;

-- DROP TABLE dbtest_exercise05;

CREATE TABLE IF NOT EXISTS dbtest_JSON (
  username VARCHAR(12) NOT NULL,
  fname VARCHAR(15),
  lname VARCHAR(20),
  email VARCHAR(50),
  PRIMARY KEY (username)
);

INSERT INTO dbtest_JSON VALUES
('jsmith25', 'John', 'Smith', 'john@live.com'),
('btaylor45', 'Bill', 'Taylor', 'bt23@gmail.com'),
('hrc15', 'Harry', 'Clinton', 'hrc23@hotmail.com'),
('pp25', 'Peter', 'Parker', 'peterp@hello.com'),
('pete15', 'Pete', 'Smith', 'psmith@live.com'),
('p3p', 'Paul', 'Peterson', 'ppp@gmail.com');


