-- Answers to Exercise 3 here

CREATE TABLE dbtest_videos (
  userid INT NOT NULL,
  fname VARCHAR(15),
  lname VARCHAR(15),
  gender CHAR (1),
  yearborn INT (4),
  joined INT (4),
  hires INT,
  PRIMARY KEY (userid)
);

INSERT INTO dbtest_videos (userid, fname, lname, gender, yearborn, joined, hires) VALUES
  (01, 'Peter', 'Jackson', 'M', 1961, 1997, 17000),
  (02, 'Jane', 'Campion', 'F', 1954, 1980, 30000),
  (03, 'Roger', 'Donaldson', 'M', 1945, 1980, 12000),
  (04, 'Temuera', 'Morrison', 'M', 1960, 1995, 15500),
  (05, 'Russell', 'Crowe', 'M', 1964, 1990, 10000),
  (06, 'Lucy', 'Lawless', 'F', 1968, 1995, 5000),
  (07, 'Michael', 'Hurst', 'M', 1957, 2000, 15000),
  (08, 'Andrew', 'Niccol', 'M', 1964, 1997, 3500),
  (09, 'Kiri', 'Te Kanawa', 'F', 1944, 1997, 500),
  (10, 'Lorde', NULL, 'F', 1996, 2010, 1000 );

SELECT fname, lname FROM dbtest_videos WHERE yearborn > 1960;
