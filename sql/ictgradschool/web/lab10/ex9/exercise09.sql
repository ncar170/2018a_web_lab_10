-- Answers to Exercise 9 here
SELECT * FROM dbtest_videos;

SELECT userid, fname, lname, gender, yearborn, joined FROM dbtest_videos;

SELECT title FROM dbtest_articles;

SELECT DISTINCT director FROM dbtest_video_store;

SELECT title FROM dbtest_video_store
WHERE rate <= 4;

SELECT fname, lname, yearborn FROM dbtest_videos
ORDER BY yearborn;

SELECT username FROM dbtest_JSON
WHERE fname LIKE 'Pete%';

SELECT username FROM dbtest_JSON
WHERE fname LIKE 'Pete%' OR lname LIKE 'Pete%';