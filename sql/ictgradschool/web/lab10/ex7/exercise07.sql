-- Answers to Exercise 7 here
CREATE TABLE IF NOT EXISTS dbtest_articles_comments (
  commentID INT (4) NOT NULL AUTO_INCREMENT,
  articleID INT (3),
  PRIMARY KEY (commentID),
  FOREIGN KEY (articleID) REFERENCES dbtest_articles (articleID)
);

ALTER TABLE dbtest_articles_comments AUTO_INCREMENT=1000;

ALTER TABLE dbtest_articles_comments ADD comment_content VARCHAR (10000) AFTER commentID;


