-- Answers to Exercise 6 here
CREATE TABLE IF NOT EXISTS dbtest_rentals (
barcode INT (9),
title VARCHAR(50),
director VARCHAR(50),
rate INT (1),
PRIMARY KEY (barcode),
FOREIGN KEY (barcode) REFERENCES dbtest_videos (userid)
);

INSERT INTO dbtest_rentals VALUES
(245789766, 'The Incredible Journey', 'Fletcher Markle', 4),
(459875623, 'All Good Things', 'Andrew Jarecki', 6),
(986784532, 'Idiocracy', 'Mike Judge', 4),
(756342789, 'Scarface', 'Brian De Palma', 8),
(428697453, 'Puss in Boots', 'Chris Miller', 6),
(234890675, 'The Expendables', 'Sly Stallone', 4),
(345964112, 'Captain America', 'Joe Johnston', 6),
(315892413, 'Top Gun', 'Tony Scott', 8),
(459871432, 'Catfish', 'Andrew Jarecki', 4),
(679409252, 'The Untouchables', 'Brian De Palma', 6),
(159875411, 'Mission: Impossible', 'Brian De Palma', 8);

DROP TABLE dbtest_rentals;

CREATE TABLE IF NOT EXISTS dbtest_video_store (
  barcode INT (9),
  title VARCHAR(50),
  director VARCHAR(50),
  rate INT (1),
  customerID INT (2),
  PRIMARY KEY (barcode),
  FOREIGN KEY (customerID) REFERENCES dbtest_videos (userid)
);

INSERT INTO dbtest_video_store VALUES
  (245789766, 'The Incredible Journey', 'Fletcher Markle', 4, 01),
  (459875623, 'All Good Things', 'Andrew Jarecki', 6, 09),
  (986784532, 'Idiocracy', 'Mike Judge', 4, 08),
  (756342789, 'Scarface', 'Brian De Palma', 8, 09),
  (428697453, 'Puss in Boots', 'Chris Miller', 6, 03),
  (234890675, 'The Expendables', 'Sly Stallone', 4, 02),
  (345964112, 'Captain America', 'Joe Johnston', 6, 05),
  (315892413, 'Top Gun', 'Tony Scott', 8, 10),
  (459871432 , 'Catfish', 'Andrew Jarecki', 4, 09),
  (679409252, 'The Untouchables', 'Brian De Palma', 6, 06),
  (159875411, 'Mission: Impossible', 'Brian De Palma', 8, 03);

SELECT customerID, title FROM dbtest_video_store;